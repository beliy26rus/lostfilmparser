﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using LostFilm.Annotations;

namespace LostFilmParser
{
    public class TvSeries:INotifyPropertyChanged
    {

        public string Name { get; set; }
        public string Url { get; set; }
        private bool _isNewSeries;
        public bool IsNewSeries
        {
            get { return _isNewSeries; }
            set
            {
                _isNewSeries = value;
                OnPropertyChanged("IsNewSeries");
            }
        }

        private List<Series> _series; 

        public List<Series> Series {
            get { return _series; }
            set
            {
                _series = value;
                OnPropertyChanged("Series");
            }
        }

        public TvSeries()
        {
            Series=new List<Series>();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
