﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using HtmlAgilityPack;
using LostFilm;

namespace LostFilmParser
{
    public class LostFilmParser
    {
        /// <summary>
        /// Get all series on Lostfilm by url
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public async Task<TvSeries> GetTvSeriesAsync(string url)
        {
            return await Task.Factory.StartNew(() =>
             {
                 try
                 {
                     var client = new WebClient();
                     var htmlString = client.DownloadString(url);
                     var htmlDocument = new HtmlDocument();
                     htmlDocument.LoadHtml(htmlString);
                     var node = htmlDocument.DocumentNode.SelectNodes("/html[1]/body[1]/div[3]/div[1]/div[4]/div[1]/div[1]/div[2]");
                     var tvSeries = new TvSeries();
                     var name = htmlDocument.DocumentNode.SelectNodes("/html/body/div[3]/div[1]/div[4]/div[1]/div[1]/div[1]/h1").First().InnerText;
                     tvSeries.Name = name;
                     tvSeries.Url = url;
                     tvSeries.IsNewSeries = false;
                     AddSeries(node, tvSeries);
                     return tvSeries;
                 }
                 catch (Exception)
                 {

                     return null;
                 }
             });

        }

        private void AddSeries(IEnumerable<HtmlNode> collection, TvSeries tvSeries)
        {
            foreach (var node in collection)
            {
                if (node.Attributes["class"] != null && node.Attributes["class"].Value == "t_row odd" | node.Attributes["class"].Value == "t_row even")
                {
                    var temp = node.ChildNodes[1].ChildNodes[1].ChildNodes[3].InnerText;
                    var name = node.ChildNodes[1].ChildNodes[3].ChildNodes[1].InnerText;
                    var date = DateTime.Parse(temp.Split(',')[0]);
                    var season = temp.Split(',')[1].TrimStart().TrimEnd().Split(' ')[0];
                    var number = string.Empty;
                    if (temp.Split(',')[1].TrimStart().TrimEnd().Split(' ').Count() >= 4)
                    {
                        number = temp.Split(',')[1].TrimStart().TrimEnd().Split(' ')[2];
                    }
                    tvSeries.Series.Add(new Series()
                    {
                        Name = name,
                        Date = date,
                        IsNewSeries = false,
                        Number = number,
                        Season = season,
                    });
                }
                AddSeries(node.ChildNodes.ToList(), tvSeries);
            }
        }

        public async Task<List<TvSeries>> GetAllTvSeries()
        {

            return await Task.Factory.StartNew(() =>
            {
                const string site = "https://www.lostfilm.tv";
                var client = new WebClient();
                var htmlString = client.DownloadString("https://www.lostfilm.tv/");
                var htmlDocument = new HtmlDocument();
                htmlDocument.LoadHtml(htmlString);
                var mainNode =
                    htmlDocument.DocumentNode.SelectNodes("/html/body/div[3]/div[1]/div[3]/div[2]/div[2]").First();
                return
                    mainNode.ChildNodes.Where(n => n.Name == "a")
                        .Select(
                            node => new TvSeries() { Name = node.InnerText, Url = site + node.Attributes["href"].Value })
                        .ToList();
            });


        }


    }
}
