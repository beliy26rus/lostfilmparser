﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using LostFilm.Annotations;

namespace LostFilmParser
{
    public class Series:INotifyPropertyChanged
    {
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string Number { get; set; }
        public string Season { get; set; }

        private bool _isNewSeries;
        public bool IsNewSeries {
            get { return _isNewSeries; }
            set
            {
                _isNewSeries = value;
                OnPropertyChanged("IsNewSeries");
            }
        }

        public override bool Equals(object obj)
        {
            var series = obj as Series;
            if (series == null) return false;
            return Name.Equals(series.Name) && Number.Equals(series.Number) && Season.Equals(series.Season);
        }

        public override int GetHashCode()
        {
            return (Name + Number + Season).GetHashCode();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
