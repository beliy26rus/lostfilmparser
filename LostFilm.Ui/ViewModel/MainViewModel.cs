using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Threading;
using System.Xml.Serialization;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using LostFilm.Ui.Serialize;
using LostFilmParser;
using Microsoft.Win32;

namespace LostFilm.Ui.ViewModel
{

    public class MainViewModel : ViewModelBase
    {
        public ObservableCollection<TvSeries> CollectionTvSeries { get; set; }
        private readonly DispatcherTimer _checkTimer;
        private readonly string _baseLocation;
        private const string RegKey = "Lostfilm";

        private DateTime _lastCheckDateTime;
        public DateTime LastCheckDateTime
        {
            get { return _lastCheckDateTime; }
            set
            {
                _lastCheckDateTime = value;
                RaisePropertyChanged(()=>LastCheckDateTime);
            }
        }
        

        private bool _error;

        private bool _startup;
        public bool Startup
        {
            get { return _startup; }
            set
            {
                _startup = value;
                SetStartup(value);
                RaisePropertyChanged(() => Startup);

            }
        }



        private int _period;
        public int Period
        {
            get { return _period; }
            set
            {
                _period = value;
                RaisePropertyChanged(() => Period);
                if (_checkTimer == null) return;
                _checkTimer.Interval = new TimeSpan(0, 0, Period, 0);
                _checkTimer.Start();
            }
        }


        private int _count;
        public int Count
        {
            get { return _count; }
            set
            {
                _count = value;
                RaisePropertyChanged(() => Count);
            }
        }

        private bool _isLoading;

        public bool IsLoading
        {
            get { return _isLoading; }
            set
            {
                _isLoading = value;
                RaisePropertyChanged(()=>IsLoading);
            }
        }
        

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                RaisePropertyChanged(() => IsBusy);
            }
        }


        private TvSeries _tvSeries;
        public TvSeries TvSeries
        {
            get { return _tvSeries; }
            set
            {
                _tvSeries = value;
                RaisePropertyChanged(() => TvSeries);
            }
        }

        private string _url;
        public string Url
        {
            get { return _url; }
            set
            {
                _url = value;
                RaisePropertyChanged(() => Url);
            }
        }


        public RelayCommand RemoveTvSeriesRelayCommand { get; private set; }
        public RelayCommand CheckNewRelayCommand { get; private set; }
        public RelayCommand CloseRelayCommand { get; private set; }
        public RelayCommand<object> TvSeriesCheckedRelayCommand { get; private set; }
        public RelayCommand<object> GoToSiteRelayCommand { get; private set; }


        private async void AddTvSeries(string url)
        {
            IsBusy = true;
            var parser = new LostFilmParser.LostFilmParser();
            var x = await parser.GetTvSeriesAsync(url);
            if (x == null)
            {
                IsBusy = false;
                Messenger.Default.Send(new NotifyMessage()
                {
                    Header = "������ ����������",
                    IsPlaySound = true,
                    Mesage = "���������� �����"
                });
                return;
            }
            CollectionTvSeries.Add(x);
            TvSeries = x;
            Url = string.Empty;
            Save();
            IsBusy = false;
            Messenger.Default.Send(new NotifyMessage()
            {
                Header = "�������� ������� ���������",
                IsPlaySound = false,
                Mesage = x.Name + " ��������"
            });
        }

        private void CountNewSeries()
        {
            Count = 0;
            foreach (var tvSeries in CollectionTvSeries)
            {
                Count += tvSeries.Series.Count(s => s.IsNewSeries);
            }

            if (_error)
            {
                Messenger.Default.Send(new NotifyMessage()
                {
                    Header = "������ ��������",
                    IsPlaySound = true,
                    Mesage = "�� ������� ��������� ������� ����� �����"
                });
                return;
            }
            LastCheckDateTime = DateTime.Now;
            if (Count > 0)
            {
                Messenger.Default.Send(new NotifyMessage()
                {
                    Header = "�������� ����� �����",
                    IsPlaySound = true,
                    Mesage = "����� �����:" + Count
                });
            }
            else
            {
                Messenger.Default.Send(new NotifyMessage()
                {
                    Header = "�������� ������ �������",
                    IsPlaySound = false,
                    Mesage = "����� ����� �� ����������"
                });
            }
        }


        private async void CheckAll()
        {
            IsLoading = true;
            if (!CollectionTvSeries.Any()) return;
            _error = false;
            await Task.Factory.StartNew(() =>
            {


                foreach (var tvSeries in CollectionTvSeries.ToList())
                {
                    if (CheckNew(tvSeries) == false)
                    {
                        _error = true;
                    }
                }
                CountNewSeries();
                Save();
            });
            IsLoading = false;

        }

        private bool CheckNew(TvSeries tvSeries)
        {
            var parser = new LostFilmParser.LostFilmParser();
            var ser = parser.GetTvSeriesAsync(tvSeries.Url).Result;
            if (ser == null) return false;
            if (tvSeries.Series.SequenceEqual(ser.Series)) return true;
            foreach (var series in ser.Series)
            {
                series.IsNewSeries = !tvSeries.Series.Contains(series) || tvSeries.Series.First(x => x.Equals(series)).IsNewSeries;
            }
            tvSeries.Series = ser.Series;
            tvSeries.IsNewSeries = tvSeries.Series.Any(s => s.IsNewSeries);
            return true;
        }

        private void Load()
        {
            if (!File.Exists(_baseLocation))
            {
                CollectionTvSeries = new ObservableCollection<TvSeries>();
                Period = 30;
                return;
            }
            var seriazer = new XmlSerializer(typeof(XmlTvSeries));
            using (var reader = new StreamReader(_baseLocation))
            {
                var xmlObject = seriazer.Deserialize(reader);
                CollectionTvSeries = new ObservableCollection<TvSeries>(((XmlTvSeries)xmlObject).TvSeries);
                Period = ((XmlTvSeries)xmlObject).Period;
            }
        }

        private void Save()
        {
            var serializer = new XmlSerializer(typeof(XmlTvSeries));
            var xmlObject = new XmlTvSeries() { TvSeries = CollectionTvSeries.ToList(), Period = Period };
            using (var writer = new StreamWriter(_baseLocation))
            {
                serializer.Serialize(writer, xmlObject);
            }
        }

        private void GoToSite(object obj)
        {
            var url = obj as string;
            if (url == null) return;
            Process.Start(url);
        }

        private void RemoveTvSeries()
        {
            if (TvSeries == null) return;
            CollectionTvSeries.Remove(TvSeries);
        }

        private void Checked(object obj)
        {
            var series = obj as Series;
            if (series == null) return;
            series.IsNewSeries = false;
            TvSeries.IsNewSeries = TvSeries.Series.Any(s => s.IsNewSeries);
            Save();
        }

        private bool IsStartup()
        {
            var rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            return rkApp.GetValue(RegKey) != null;
        }

        private void SetStartup(bool startup)
        {
            var rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

            if (startup)
            {

                rkApp.SetValue(RegKey, Assembly.GetExecutingAssembly().Location + " /hide");
            }
            else
            {
                rkApp.DeleteValue(RegKey, false);
            }
        }



        public MainViewModel()
        {
            _baseLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\base.xml";
            Messenger.Default.Register<TvSeries>(this, (TvSeries tv) =>
            {
                if (tv == null) return;
                AddTvSeries(tv.Url);
            });
            RemoveTvSeriesRelayCommand = new RelayCommand(RemoveTvSeries, () => TvSeries != null);
            CheckNewRelayCommand = new RelayCommand(CheckAll, () => CollectionTvSeries.Any());
            CloseRelayCommand = new RelayCommand(Save);
            TvSeriesCheckedRelayCommand = new RelayCommand<object>(Checked);
            GoToSiteRelayCommand = new RelayCommand<object>(GoToSite);
            Load();
            Startup = IsStartup();
            CountNewSeries();
            _checkTimer = new DispatcherTimer { Interval = new TimeSpan(0, 0, Period, 0) };
            _checkTimer.Tick += (sender, args) => { CheckAll(); };
            _checkTimer.Start();
            var startTimer = new DispatcherTimer() { Interval = new TimeSpan(0, 0, 0, 10) };
            startTimer.Start();
            startTimer.Tick += (sender, args) =>
            {
                CheckAll();
                startTimer.Stop();

            };


        }
    }
}