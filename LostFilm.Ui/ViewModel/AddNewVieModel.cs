﻿using System.Collections.Generic;
using System.Linq;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using LostFilmParser;

namespace LostFilm.Ui.ViewModel
{
    public class AddNewVieModel : ViewModelBase
    {
        private bool _isBusy;

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value; 
                RaisePropertyChanged(()=>IsBusy);
            }
        }
        

        private string _searchString;

        public string SearchString
        {
            get { return _searchString; }
            set
            {
                _searchString = value;
                RaisePropertyChanged(()=>SearchString);
                ListTvseries = _list.Where(s => s.Name.ToLower().Contains(SearchString.ToLower())).ToList();
            }
        }
        
        

        private List<TvSeries> _list; 

        private List<TvSeries> _listTvSeries;

        public List<TvSeries> ListTvseries
        {
            get { return _listTvSeries; }
            set
            {
                _listTvSeries = value;
                RaisePropertyChanged(() => ListTvseries);
                AddRelayCommand.RaiseCanExecuteChanged();
            }
        }

        private TvSeries _tvSeries;

        public TvSeries TvSeries
        {
            get { return _tvSeries; }
            set
            {
                _tvSeries = value;
                RaisePropertyChanged(() => TvSeries);
            }
        }

        public RelayCommand AddRelayCommand { get; private set; }


        private async void GetAllTvSeries()
        {
            IsBusy = true;
            var parser = new LostFilmParser.LostFilmParser();
            _list = await parser.GetAllTvSeries();
            ListTvseries = _list;
            IsBusy = false;
        }

        public AddNewVieModel()
        {
            AddRelayCommand=new RelayCommand(() => {Messenger.Default.Send<TvSeries>(TvSeries);},()=>TvSeries!=null);
            GetAllTvSeries();
        }
    }
}
