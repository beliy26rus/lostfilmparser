﻿using System.Windows;

namespace LostFilm.Ui
{
    /// <summary>
    /// Interaction logic for AddNewWindow.xaml
    /// </summary>
    public partial class AddNewWindow : Window
    {
        public AddNewWindow()
        {
            InitializeComponent();
        }

        private void ButtonCancel_OnClick(object sender, RoutedEventArgs e)
        {
           Close();
        }

        private void ButtonOk_OnClick(object sender, RoutedEventArgs e)
        {
           Close();
        }
    }
}
