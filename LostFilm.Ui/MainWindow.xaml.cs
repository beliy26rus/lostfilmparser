﻿using System;
using System.ComponentModel;
using System.Media;
using System.Windows;
using GalaSoft.MvvmLight.Messaging;

namespace LostFilm.Ui
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow 
    {


        public MainWindow()
        {
            InitializeComponent();
            Messenger.Default.Register<NotifyMessage>(this, ShowStandardBalloon);
        }


        private void ShowStandardBalloon(NotifyMessage message)
        {
            if (message.IsPlaySound)
                SystemSounds.Asterisk.Play();
            TaskbarIcon.ShowBalloonTip(message.Header, message.Mesage, TaskbarIcon.Icon);


        }

        private void MainWindow_OnStateChanged(object sender, EventArgs e)
        {
            ShowInTaskbar = WindowState != WindowState.Minimized;
        }

        private void MenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState == WindowState.Minimized ? WindowState.Normal : WindowState.Minimized;
        }

        private void MenuItemClose_OnClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void TaskbarIcon_OnTrayLeftMouseDown(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Normal;
            ShowInTaskbar = true;


        }

        private void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            var args = Environment.GetCommandLineArgs();
            var startMinimized = false;
            for (var i = 0; i != args.Length; ++i)
            {
                if (args[i] == "/hide")
                {
                    startMinimized = true;
                }
            }
            if (!startMinimized) return;
            WindowState = WindowState.Minimized;
            ShowInTaskbar = false;
        }

        private void MainWindow_OnClosing(object sender, CancelEventArgs e)
        {
            var messagewindow = new MessageWindow();

            var showDialog = messagewindow.ShowDialog();
            if ( showDialog != null && !(bool) showDialog)
                e.Cancel=true;
            
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var window = new AddNewWindow() {Owner = this};
            window.ShowDialog();
        }
    }
}
