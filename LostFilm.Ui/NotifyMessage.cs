﻿namespace LostFilm.Ui
{
    class NotifyMessage
    {
        public string Header { get; set; }
        public string Mesage { get; set; }
        public bool IsPlaySound { get; set; }
    }
}
