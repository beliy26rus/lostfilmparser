﻿using System;
using System.Collections.Generic;
using LostFilmParser;


namespace LostFilm.Ui.Serialize
{
    [Serializable]
    public class XmlTvSeries
    {
        public int Period { get; set; }
        public List<TvSeries> TvSeries { get; set; }
    }
}
