﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace LostFilm.Ui.Converter
{
   public class FullSeasonBorderBrushConverter:IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (string.IsNullOrEmpty((string)value))
            {
                return new SolidColorBrush(Colors.BlueViolet);
            }
            return new SolidColorBrush(Colors.Orange);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
